<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * test 1
     * @return Response|null
     */
    public function indexAction()
    {
        $array = ["r" => 2,"r" => "e"] ;
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'thing' => $array
        ]);
    }

    /**
     * @return void
     */
    public function text()
    {
    }
}
